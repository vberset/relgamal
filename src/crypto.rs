use {
    blake2::{Blake2b, Digest},
    num_bigint::{BigInt, BigUint, RandBigInt, ToBigInt},
    num_integer::Integer,
    num_iter::{range, range_inclusive},
    num_traits::{One, Pow, Signed, Zero},
    rand,
    std::{fmt, fs::File, io, path::Path},
};

/// ElGamal signature
pub struct Signature {
    r: BigUint,
    s: BigUint,
}

impl fmt::Display for Signature {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        write!(f, "Signature {{\n    r: {},\n    s: {}\n}}", self.r, self.s)
    }
}

/// ElGamal signature scheme crypto system.
pub struct CryptoSystem {
    x: BigUint, // Private key
    p: BigUint, // Public key's safe prime
    g: BigUint, // Public key's generator
    y: BigUint, // Public key's "exponentiation"
}

impl CryptoSystem {
    /// Generate new random values for the ElGamal signature scheme
    /// with `p` of the specified size in bits.
    pub fn new(bit_size: usize) -> CryptoSystem {
        let q = generate_prime(bit_size >> 1);
        let p = generate_schnorr_prime(&q, bit_size);
        let g = generate_generator(&p, &q);
        let x = generate_private_key(&p, &g);
        let y = g.modpow(&x, &p);

        CryptoSystem { x, p, g, y }
    }

    /// Generate a signature for the specified message, using the
    /// CryptoSystem's private key, verifiable by the CryptoSystem's public key.
    pub fn sign(&self, m: &BigUint) -> Signature {
        let one = BigUint::one();
        let p_minus_one = &self.p - BigUint::one();

        let (r, s) = loop {
            // Choose a random k such that 0 < k < p − 1 and gcd(k, p − 1) = 1.
            let k = loop {
                let k = generate_prime(self.p.bits() - 1);
                // k and p-1 must be coprime
                if k.gcd(&p_minus_one) == one {
                    break k;
                }
            };

            // Compute r ≡ g^k (mod p).
            let r = self.g.modpow(&k, &self.p);

            // Compute s ≡ (H(m)−xr)k⁻¹) (mod p−1)
            if let Some(k_inv) = modular_inverse(&k, &p_minus_one) {
                let x_r = &self.x * &r % &p_minus_one;
                let m_x_r = if &x_r > m {
                    &p_minus_one - x_r + m
                } else {
                    m - x_r
                };
                let s = &(&m_x_r * &k_inv) % &p_minus_one;
                if !s.is_zero() {
                    break (r, s);
                }
            }
        };

        Signature { r, s }
    }

    /// Check the validity of the specified signature for the given file
    /// using the public key.
    pub fn verify(&self, m: &BigUint, signature: &Signature) -> bool {
        let v1 = self.g.modpow(m, &self.p);
        let v2 = self.y.modpow(&signature.r, &self.p)
            * signature.r.modpow(&signature.s, &self.p)
            % &self.p;

        v1 == v2
    }
}

impl fmt::Display for CryptoSystem {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        write!(f, "CryptoSystem {{\n    \
            private key: {},\n    \
            public key: {{\n        \
            p: {},\n        \
            g: {},\n        \
            y: {},\n    \
            }}\n\
        }}", self.x, self.p, self.g, self.y)
    }
}

/// Extended euclid algorithm
///
/// Compute r, x, and y, as r = gdc(a, b) = xa + yb.
fn extended_euclid(a: &BigInt, b: &BigInt) -> (BigInt, BigInt, BigInt) {
    let zero = BigInt::zero();
    let one = BigInt::one();

    if b == &zero {
        return (a.clone(), one, zero);
    } else {
        let (quo, rem) = a.div_rem(b);
        let (r, x, y) = extended_euclid(b, &rem);
        let z = x - quo * &y;
        return (r, y, z);
    }
}

/// Modular multiplicative inverse
fn modular_inverse(a: &BigUint, p: &BigUint) -> Option<BigUint> {
    let one = BigInt::one();
    let a = a.to_bigint().unwrap();
    let p = p.to_bigint().unwrap();

    let (r, x, _) = extended_euclid(&a, &p);

    if r != one {
        None
    } else if x.is_negative() {
        Some(x + p)
    } else {
        Some(x)
    }.map(|x| x.to_biguint().unwrap())
}

/// Factor ``n``, an even integer, to `r` and `q` as n = 2<sup>r</sup> · q ,
/// with q odd.
fn factor_two(n: &BigUint) -> (BigUint, BigUint) {
    assert!(n.is_even());

    let mut q = (*n).clone();
    let mut r = BigUint::zero();
    let one = BigUint::one();

    while &q & &one != one {
        r += &one;
        q >>= 1;
    }

    (r, q)
}

/// Check for composite witnesses
fn witness_composite(p: &BigUint, x: &BigUint) -> bool {
    let zero = BigUint::zero();
    let one = BigUint::one();
    let two = 2u8.into();
    let p_minus_one = p - &one;
    let (r, q) = factor_two(&p_minus_one);
    let mut y = x.modpow(&q, p);

    if y == one {
        return false;
    }

    for _ in range_inclusive(zero, r) {
        if &y == &p_minus_one {
            return false;
        }

        y = y.modpow(&two, &p);

        if y == one {
            return true;
        }
    }

    return true;
}

/// Miller-Rabin primality test
fn likely_prime(p: &BigUint, t: usize) -> bool {
    let lbound: BigUint = 2u32.into();
    let ubound = p.clone();
    let mut rng = rand::thread_rng();

    // returns false if p is even or is 1
    if p.is_even() || ubound < lbound {
        return false;
    }

    for _ in 0..t {
        let x = rng.gen_biguint_range(&lbound, &ubound);
        if witness_composite(p, &x) {
            return false;
        }
    }

    return true;
}

/// Generate a random prime of the specified bit size.
pub fn generate_prime(bit_size: usize) -> BigUint {
    let mut rng = rand::thread_rng();

    loop {
        let x: BigUint = rng.gen_biguint(bit_size);

        if likely_prime(&x, 30) {
            return x;
        }
    }
}

/// Generate a Schnorr prime of the specified bit size.
///
/// Given a prime value ``q``, search for a large prime of ``bit_size`` bits
/// as $ p = kq + 1 $.
pub fn generate_schnorr_prime(q: &BigUint, bit_size: usize) -> BigUint {
    let one = BigUint::one();
    let two: BigUint = 2u32.into();
    let lbound = (&two).pow(bit_size - q.bits());
    let ubound = (&two).pow(bit_size - q.bits() + 1) - &one;

    for k in range(lbound, ubound) {
        let p = k * q + &one;
        if likely_prime(&p, 30) {
            return p;
        }
    }

    unreachable!();
}

/// Generate a random generator of ℤ<sup>×</sup><sub>p</sub> order q.
pub fn generate_generator(p: &BigUint, q: &BigUint) -> BigUint {
    let mut rng = rand::thread_rng();

    let one = BigUint::one();
    let two: BigUint = 2u32.into();
    let p_minus_one = p - &one;

    loop {
        let h = rng.gen_biguint_range(&two, &p_minus_one);
        let g = h.modpow(&(&p_minus_one / q), p);
        if g != one {
            return g;
        }
    }
}

/// Generate a private key, with it and g coprime.
pub fn generate_private_key(p: &BigUint, g: &BigUint) -> BigUint {
    let mut rng = rand::thread_rng();

    let one = BigUint::one();
    let two: BigUint = 2u32.into();
    let p_minus_one = p - &one;

    loop {
        let x = rng.gen_biguint_range(&two, &p_minus_one);

        if x.gcd(g) == one {
            return x;
        }
    }
}

/// Compute the blake2b 512 bits hash of the specified file.
pub fn hash_reader<P: AsRef<Path>>(file_name: P) -> io::Result<BigUint> {
    let mut file = File::open(file_name)?;
    let mut hasher = Blake2b::new();

    io::copy(&mut file, &mut hasher)?;

    Ok(BigUint::from_bytes_be(hasher.result().as_slice()))
}

#[cfg(test)]
mod tests {
    use num_iter::range;
    use quickcheck_macros::quickcheck;

    use super::*;

    #[quickcheck]
    fn test_cryptosystem(bytes: Vec<u8>) {
        let cs = CryptoSystem::new(32);
        let message = BigUint::from_bytes_le(bytes.as_slice());

        let signature = cs.sign(&message);
        assert!(cs.verify(&message, &signature));
    }

    #[quickcheck]
    fn test_corrupted_signature_on_r(bytes: Vec<u8>) {
        let cs = CryptoSystem::new(32);
        let message = BigUint::from_bytes_le(bytes.as_slice());

        let mut signature = cs.sign(&message);
        signature.r += 1u32;
        assert!(!cs.verify(&message, &signature));
    }

    #[quickcheck]
    fn test_corrupted_signature_on_s(bytes: Vec<u8>) {
        let cs = CryptoSystem::new(32);
        let message = BigUint::from_bytes_le(bytes.as_slice());

        let mut signature = cs.sign(&message);
        signature.s += 1u32;
        assert!(!cs.verify(&message, &signature));
    }

    #[test]
    fn test_extended_euclid() {
        let one = BigInt::one();
        let a = 3u32.into();
        let b = 352u32.into();

        let (r, x, y) = extended_euclid(&a, &b);

        assert_eq!(r, one);
        assert_eq!(x, (-117).into());
        assert_eq!(y, one);
    }

    #[test]
    fn test_factor_two() {
        let (r, q) = factor_two(&100u64.into());
        assert_eq!(r, 2u64.into());
        assert_eq!(q, 25u64.into());

        let (r, q) = factor_two(&346u64.into());
        assert_eq!(r, 1u64.into());
        assert_eq!(q, 173u64.into());
    }

    #[test]
    fn test_witness() {
        let two: BigUint = 2u8.into();
        let p: BigUint = 101u8.into();

        for x in range(two, p.clone()) {
            assert_eq!(witness_composite(&p, &x), false);
        }
    }

    #[test]
    fn test_primality() {
        assert_eq!(likely_prime(&(99u32.into()), 10), false);
        assert_eq!(likely_prime(&(100u32.into()), 10), false);
        assert_eq!(likely_prime(&(101u32.into()), 10), true);

        assert_eq!(likely_prime(&(7917u32.into()), 16), false);
        assert_eq!(likely_prime(&(7919u32.into()), 16), true);

        // Primes generated with OpenSSL
        assert_eq!(likely_prime(&(12730097u32.into()), 10), true);
        assert_eq!(likely_prime(&(1903537451u32.into()), 10), true);

        assert_eq!(likely_prime(&(1903537453u32.into()), 10), false);
    }
}
