mod crypto;

use crypto::*;
use clap::{App, Arg};

fn main() {
    let matches = App::new("relgamal")
        .version("1.0")
        .author("Vincent Berset <vberset@protonmail.ch")
        .about("ElGamal signature scheme toy implementation")
        .arg(Arg::with_name("key-size")
            .short("s")
            .long("key-size")
            .value_name("SIZE")
            .help("Bit size of the generated key (default: 512)")
            .takes_value(true))
        .arg(Arg::with_name("FILE")
            .help("File to sign")
            .required(true)
            .index(1))
        .get_matches();

    let key_size = matches.value_of("key-size")
        .unwrap_or("512")
        .parse::<usize>()
        .expect("invalid key size: must be a positive number");

    let file = matches.value_of("FILE").unwrap();

    if key_size < 8 {
        panic!("invalid key size: must be >= 8");
    }

    // Hash the input file
    let m = hash_reader(&file).unwrap();
    println!("Message: {}\n", &m);

    // Generate private and public keys
    let cs = CryptoSystem::new(key_size);
    println!("{}\n", &cs);

    // Compute a signature
    let signature = cs.sign(&m);
    println!("{}\n", &signature);

    // Check the signature validity
    if cs.verify(&m, &signature) {
        println!("✔️ Valid signature");
        std::process::exit(0);
    } else {
        println!("✖️ Invalid signature");
        std::process::exit(1);
    }
}
