# relgamal

Toy implementation of the [ElGamal signature scheme](https://en.wikipedia.org/wiki/ElGamal_signature_scheme)
using the [Rust programming language](https://rust-lang.org).

At each run, `relgamal` generates a key pair and display it. Then it computes
a signature based on the private key and a secret random `k` for the specified
file and display it as well. Finally, it verifies that signature using
the public key.

## Build

We assume that the rust toolchain is already installed on your system.
If not, [rustup](https://rustup.rs/) is the recommended way to do so.

To build the executable, simply run:

```
$ cargo build --release
```

The generated executable sit on the `target/release` folder.

## Usage

Simply run:

```
$ relgamal file-to-sign
```

A key size can be specified in bits with the `--key-size` long option, or
the `-s` short option:

```
$ relgamal -s 2048 file-to-sign
```

The default key size is 512 bits.

### Output example

Example invocation:

```
$ relgamal -s 32 src/main.rs
```

The output will look like this:

```
CryptoSystem {
    private key: 207795494,
    public key: {
        p: 2347654993,
        g: 532551911,
        y: 425517998,
    }
}

Signature {
    r: 1825465421,
    s: 1683597535
}

✔️ Valid signature
```

## Implementation details

The primality test is based on the probabilist
[Miller-Rabin algorithm](https://en.wikipedia.org/wiki/Miller%E2%80%93Rabin_primality_test).
The number of iteration is set to 30, thus the probability of false positive is $`\left(1/4\right)^{30}`$.

A prime $`q`$ is generated and used to build a safe prime $`p`$,
such that $`p = qk + 1`$.
So we have a [Schnorr group](https://en.wikipedia.org/wiki/Schnorr_group),
allowing us to build a generator $`g`$ with $`g = h^r \mod p`$ where $`h^r \not\equiv 1 \left(\mod p\right)`$.
$`g`$ is a generator of a subgroup of $`\mathbb{Z}^×_p`$ of order $`q`$. 

A private key $`a`$ is generated, such as $`1 < a < p-2`$
and $`a`$ is relatively prime to $`q`$.

## External resources

* [Handbook of Applied Cryptography](http://cacr.uwaterloo.ca/hac/)
  by Alfred J. Menezes, Paul C. van Oorschot and Scott A. Vanstone, CRC Press
* [Real-World Algorithms](https://mitpress.mit.edu/books/real-world-algorithms)
  by Panos Louridas, MIT Press